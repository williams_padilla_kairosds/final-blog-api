FROM node:16-alpine3.15 AS builder
WORKDIR /builder
COPY . .
RUN npm install
RUN npm run build 
RUN npm prune --production

FROM node:16-alpine3.15 AS production

WORKDIR /app

COPY . .
COPY --from=builder /builder/package.json ./package.json
COPY --from=builder /builder/package-lock.json ./package-lock.json
COPY --from=builder /builder/dist ./dist
COPY --from=builder /builder/node_modules ./node_modules

EXPOSE 3001

CMD [ "node", "dist/app" ]


