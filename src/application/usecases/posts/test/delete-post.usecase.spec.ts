jest.mock('../../../../infrastructure/repositories/post.repository.pg', () => {
    return {
        PostRepositoryPG: jest.fn().mockImplementation(() => {
            return {
                getById: jest.fn().mockImplementation(() => {
                    return new Post({
                        id: IdVO.createWithUUID(
                            '1a0fde8a-e773-40c4-a39e-701cc994da18'
                        ),
                        authorName: NickNameVO.create('StarFlow'),
                        title: TitleVO.create('Hi how are you'),
                        text: TextVO.create(
                            'I was wondeasdasdasdasdasdasdawwdasdwqfqfqwfqwfqwfqfqwfqwfqwfqwfwq...'
                        ),
                    }); 
                }),
                delete: jest.fn()
            };
        })
    };
});

// jest.mock('./../../../../infrastructure/repositories/user-repository.pg');


import 'reflect-metadata';
import Container from 'typedi';
import { Post } from '../../../../domain/entities/post.entity';
import { TextVO } from '../../../../domain/vos/author/text.vo';
import { TitleVO } from '../../../../domain/vos/author/title.vo';
import { IdVO } from '../../../../domain/vos/id.vo';
import { NickNameVO } from '../../../../domain/vos/nickname.vo';
import { PostRepositoryPG } from '../../../../infrastructure/repositories/post.repository.pg';
import { DeletePostUseCase } from './../delete-post.usecase';
describe('Delete a post use case ', () => {

    it('should delete a post',   async () => {
        
        const repository = new PostRepositoryPG();
        Container.set('PostRepository', repository);

        const useCase = Container.get(DeletePostUseCase);
        const id = '1a0fde8a-e773-40c4-a39e-701cc994da18';
        const userId = '1cca625c-0502-42af-95b8-9f6ab6a9be7f';

        const response = await useCase.execute(userId, id);
        console.log('response del detete',response);
        expect(repository.delete).toHaveBeenCalled();
    });
});