jest.mock('../../../../infrastructure/repositories/post.repository.pg', () => {
    return {
        PostRepositoryPG: jest.fn().mockImplementation(() => {
            return {
                updatePost: jest.fn().mockImplementation(() => {
                    return new Post({
                        id: IdVO.create(),
                        authorName: NickNameVO.create('StarFlow'),
                        title: TitleVO.create('Hi how are you'),
                        text: TextVO.create(
                            'I was wondeasdasdasdasdasdasdawwdasdwqfqfqwfqwfqwfqfqwfqwfqwfqwfwq...'
                        ),
                    });
                }),
                getById: jest.fn()
            };
        })
    };
});


import 'reflect-metadata';
import Container from 'typedi';
import { Post } from '../../../../domain/entities/post.entity';
import { TextVO } from '../../../../domain/vos/author/text.vo';
import { TitleVO } from '../../../../domain/vos/author/title.vo';
import { IdVO } from '../../../../domain/vos/id.vo';
import { NickNameVO } from '../../../../domain/vos/nickname.vo';
import { PostRepositoryPG } from '../../../../infrastructure/repositories/post.repository.pg';
import { PostRequest } from '../post.interfaces';
import { UpdatePostUseCase } from './../update-post.usecase';
describe('Update post use case', () => {

    it('should update a post', async () => {
        
        const repository = new PostRepositoryPG();
        Container.set('PostRepository', repository);

        const useCase = Container.get(UpdatePostUseCase);

        const input: PostRequest = {
            author: 'StarFlow',
            title: 'Hi how are you',
            text: 'I was wondeasdasdasdasdasdasdawwdasdwqfqfqwfqwfqwfqfqwfqwfqwfqwfwq...',
        };

        const id = '1cca625c-0502-42af-95b8-9f6ab6a9be7f';


        const response = await useCase.execute(id, id , input);
        console.log(response);

        expect(repository.updatePost).toHaveBeenCalled();
    });
});