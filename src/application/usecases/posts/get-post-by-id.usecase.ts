import { Service } from 'typedi';
import { PostService } from '../../../domain/services/post.service';
import { IdVO } from '../../../domain/vos/id.vo';
import { PostResponse } from './post.interfaces';

@Service()
export class GetPostByIdUseCase {

    constructor(private postService: PostService){}

    async execute( id: string): Promise<PostResponse> {
        const isUUID = IdVO.createWithUUID(id);
        const response = await this.postService.getById( isUUID);
        console.log('response en el execute de get by id', response);
        const postResponse: PostResponse = {
            id: response.id,
            author: response.authorName,
            title: response.title,
            text: response.text,
            comments: response.comment?.map((comment: any) => {
                return {
                    id: comment.id,
                    authorId: comment.authorId,
                    author: comment.author,
                    content: comment.content,
                };
            }),
        };
        return postResponse;
    }
}