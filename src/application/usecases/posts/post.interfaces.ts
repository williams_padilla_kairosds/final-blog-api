
export interface PostRequest {
    author: string;
    title: string;
    text: string;
}

export interface PostResponse {
    id: string;
    author: string;
    title: string;
    text: string
    comments?: Array<any>
}

