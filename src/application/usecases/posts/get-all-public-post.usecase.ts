import { Service } from 'typedi';
import { PostService } from '../../../domain/services/post.service';
import { PostResponse } from './post.interfaces';


@Service()
export class GetAllPublicPostsUseCase {

    constructor(private postService: PostService){}

    async execute(): Promise<PostResponse[]>{
        const response = await this.postService.getAllUsersPosts();
        const output: PostResponse[] = response.map((post: any) => {
            return {
                id: post.id,
                author: post.authorName,
                title: post.title,
                text: post.text,
            };
        });
        return output;
    }
}