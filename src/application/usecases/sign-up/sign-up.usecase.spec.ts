jest.mock('./../../../infrastructure/repositories/user-repository.pg', () => {
    return {
        UserRepositoryPG: jest.fn().mockImplementation(() => {
            return {
                save: jest.fn()
            };
        })
    };
});


import 'reflect-metadata';
import Container from 'typedi';
import { Role } from '../../../domain/vos/role.vo';
import { UserRepositoryPG } from './../../../infrastructure/repositories/user-repository.pg';
import { SignUpRequest, SignUpUseCase } from './sign-up.usecase';

describe('Sign up use case', () => {

    it('should create a new user', async () => {

        const repository = new UserRepositoryPG();
        Container.set('UserRepository', repository);

        const useCase = Container.get(SignUpUseCase);

        const input: SignUpRequest = {
            email: 'hola-test-admin@testing.com',
            password: '1234',
            role: 'ADMIN' as Role,
            nickName: 'StarFlow'
        };

        await useCase.execute(input);

        expect(repository.save).toHaveBeenCalled();
    });
});