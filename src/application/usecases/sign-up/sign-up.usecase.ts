import { Service } from 'typedi';
import { User, UserType } from '../../../domain/entities/user.entity';
import { UserService } from '../../../domain/services/user.service';
import { EmailVo } from '../../../domain/vos/email.vo';
import { IdVO } from '../../../domain/vos/id.vo';
import { NickNameVO } from '../../../domain/vos/nickname.vo';
import { PasswordVo } from '../../../domain/vos/password.entity';
import { Role, RoleVo } from '../../../domain/vos/role.vo';

@Service()
export class SignUpUseCase  {

    constructor(private userService: UserService){}

    async execute(request: SignUpRequest): Promise<void> {

        console.log('aqui esta req role',request.role);

        const user: UserType = {
            id: IdVO.create(),
            email: EmailVo.create(request.email),
            password: PasswordVo.create(request.password),
            role: RoleVo.create(request.role as Role),
            nickName: NickNameVO.create(request.nickName)
        } ;

        await this.userService.persist(new User(user));
    }
}

export type SignUpRequest = {
    email: string;
    password: string;
    role: Role
    nickName: string;

}