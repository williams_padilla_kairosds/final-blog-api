import { Service } from 'typedi';
import { OffensiveWordService } from '../../domain/services/offensive-word.service';
import { OffensiveWordResponse } from './offensive-word.response';

@Service()
export class GetAllOffensiveWordsUseCase {
    constructor(private offensiveWordService: OffensiveWordService) {}

    async execute(): Promise<OffensiveWordResponse[]> {
        const response = await this.offensiveWordService.getAll();
        const offensiveWordDto: OffensiveWordResponse[] = response.map((ow) => {
            return {
                id: ow.id,
                word: ow.word,
                level: ow.level,
            };
        });
        return offensiveWordDto;
    }
}
