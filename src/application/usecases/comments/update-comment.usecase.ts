import { Service } from 'typedi';
import { CommentType } from '../../../domain/entities/comment.entity';
import { PostService } from '../../../domain/services/post.service';
import { CommentContentVO } from '../../../domain/vos/comments/date.vo';
import { IdVO } from '../../../domain/vos/id.vo';
import { NickNameVO } from '../../../domain/vos/nickname.vo';
import { CommentInput, CommentOutput } from './comment.interface';

@Service()
export class UpdateCommentUseCase {
    constructor(private postService: PostService) {}

    async execute(
        userId: string,
        postId: string,
        commentId: string,
        comment: CommentInput
    ): Promise<CommentOutput> {
        const commentType: CommentType = {
            id: IdVO.createWithUUID(commentId),
            author: NickNameVO.create(comment.author),
            content: CommentContentVO.create(comment.content),
        };

        const response = await this.postService.updateComment(
            userId,
            postId,
            commentType
        );

        const { id, authorId, author, content }: CommentOutput = response;

        return { id, authorId, author, content };
    }
}
