import { Service } from 'typedi';
import { PostService } from '../../../domain/services/post.service';
import { CommentOutput } from './comment.interface';

@Service()
export class GetAllCommentsUseCase {

    constructor(private postService: PostService){}

    async execute(userId:string, postId: string): Promise<CommentOutput[]>{
    
        const response = await this.postService.getAllComments(userId, postId);
        const output:CommentOutput[] = response.map(comment => {
            return {
                id: comment.id,
                author:comment.author,
                content: comment.content
            };
        });
        return output;
    }
}