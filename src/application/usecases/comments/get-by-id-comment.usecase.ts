import { Service } from 'typedi';
import { PostService } from '../../../domain/services/post.service';
import { IdVO } from '../../../domain/vos/id.vo';
import { CommentOutput } from './comment.interface';

@Service()
export class GetCommentByIdUseCase {

    constructor(private postService: PostService){}

    async execute(userId:string, postId:string, commentId: string): Promise<CommentOutput>{
        const isUUID = IdVO.createWithUUID(commentId);
        const response = await this.postService.getCommentById(userId, postId, isUUID);
        const {id,authorId, author, content }:CommentOutput = response;
        return {id,authorId, author, content};
    }
}