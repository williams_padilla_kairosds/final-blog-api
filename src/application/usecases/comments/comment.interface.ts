export type CommentInput = {
    author: string;
    content: string;
}

export type CommentOutput = {
    id: string,
    authorId?: string;
    author: string;
    content: string;
}