import { Service } from 'typedi';
import { OffensiveWordType } from '../../domain/entities/offensive-word.entity';
import { OffensiveWordService } from '../../domain/services/offensive-word.service';
import { IdVO } from '../../domain/vos/id.vo';
import { LevelVO } from '../../domain/vos/level.vo';
import { WordVO } from '../../domain/vos/word.vo';
import { OffensiveWordRequest } from './offensive-word.request';
import { OffensiveWordResponse } from './offensive-word.response';

@Service()
export class CreateOffensiveWordUseCase {

    constructor(private offensiveWordService: OffensiveWordService){}

    async execute(offensiveWordRequest: OffensiveWordRequest): Promise<OffensiveWordResponse>{
        const offensiveWordData: OffensiveWordType = {
            id: IdVO.create(),
            word: WordVO.create(offensiveWordRequest.word),
            level: LevelVO.create(offensiveWordRequest.level)
        };
        const response = await this.offensiveWordService.persist(offensiveWordData);
        const sentResponse: OffensiveWordResponse = {
            id: response.id,
            word: response.word,
            level: response.level,
        };
        return sentResponse;
    }
}