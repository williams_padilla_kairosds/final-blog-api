jest.mock('./../../../infrastructure/repositories/user-repository.pg', () => {
    return {
        UserRepositoryPG: jest.fn().mockImplementation(() => {
            return {
                getByEmail: jest.fn().mockImplementation(() => {
                    return new User({
                        id: IdVO.create(),
                        email: EmailVo.create('hola-login@testing.com'),
                        password: PasswordVo.createWithHash('1234'),
                        role: RoleVo.create(Role.ADMIN),
                        nickName: NickNameVO.create('StarFlow'),
                    });
                }),
            };
        }),
    };
});



import 'reflect-metadata';
import Container from 'typedi';
import { User } from '../../../domain/entities/user.entity';
import { EmailVo } from '../../../domain/vos/email.vo';
import { IdVO } from '../../../domain/vos/id.vo';
import { NickNameVO } from '../../../domain/vos/nickname.vo';
import { PasswordVo } from '../../../domain/vos/password.entity';
import { Role, RoleVo } from '../../../domain/vos/role.vo';
import { UserRepositoryPG } from '../../../infrastructure/repositories/user-repository.pg';
import { SignInRequest, SignInUseCase } from './sign-in.usecase';
describe('Sign in use case', () => {
    it('should login', async () => {
        const repository = new UserRepositoryPG();
        Container.set('UserRepository', repository);

        const useCase = Container.get(SignInUseCase);
        const loginData: SignInRequest = {
            email: 'hola-login@testing.com',
            password: '1234',
        };

        const token = await useCase.execute(loginData);
        expect(token).not.toBeNull();
        expect(repository.getByEmail).toHaveBeenCalled();
    });
});
