import jwt from 'jsonwebtoken';
import { Service } from 'typedi';
import { ExceptionWithCode } from '../../../domain/execption-with-code';
import { UserService } from '../../../domain/services/user.service';
import { EmailVo } from '../../../domain/vos/email.vo';
import { PasswordVo } from '../../../domain/vos/password.entity';

@Service()
export class SignInUseCase {

    constructor(private userService: UserService){}

    async execute(request: SignInRequest): Promise<string | null> {
        const user = await this.userService.getByEmail(EmailVo.create(request.email));
        if(!user) throw new ExceptionWithCode(404, 'User not found');

        // console.log('user en execute',user);
        const password = PasswordVo.create(request.password);
        const isValid = await this.userService.isValidPassword(password, user);

        
        if(isValid){
            return jwt.sign({email: user.email.value, role: user.role.value}, 'secret',{
                expiresIn: 86400 //24hrs     
            });
        }
        return null;
    }
}

export type SignInRequest = {
    email: string;
    password: string;
}