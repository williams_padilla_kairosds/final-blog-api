import { Service } from 'typedi';
import { OffensiveWordService } from '../../domain/services/offensive-word.service';
import { IdVO } from '../../domain/vos/id.vo';

@Service()
export class DeleteOffensiveWordUseCase {

    constructor(private offensiveWordService: OffensiveWordService){}

    async execute(id: string):Promise<IdVO>{
        const isUUID = IdVO.createWithUUID(id);
        await this.offensiveWordService.deleteById(isUUID);
        return isUUID;
    }
}