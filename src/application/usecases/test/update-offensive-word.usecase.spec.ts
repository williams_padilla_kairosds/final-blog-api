jest.mock('../../../infrastructure/repositories/offensive-word.repository.mongo', () => {
    return {
        OffensiveWordRepositoryMongo: jest.fn().mockImplementation(() => {
            return {
                update: jest.fn().mockImplementation(() => {
                    return new OffensiveWord({
                        id: IdVO.createWithUUID('1a0fde8a-e773-40c4-a39e-701cc994da18') ,
                        word: WordVO.create('feo'),
                        level: LevelVO.create(3),
                    });
                }),
                getById: jest.fn().mockImplementation(()=> {
                    return new OffensiveWord({
                        id: IdVO.createWithUUID(
                            '1a0fde8a-e773-40c4-a39e-701cc994da18'
                        ),
                        word: WordVO.create('feo'),
                        level: LevelVO.create(3),
                    });
                })
                
            };
        })
    };
});


import 'reflect-metadata';
import Container from 'typedi';
import { OffensiveWord } from '../../../domain/entities/offensive-word.entity';
import { IdVO } from '../../../domain/vos/id.vo';
import { LevelVO } from '../../../domain/vos/level.vo';
import { WordVO } from '../../../domain/vos/word.vo';
import { OffensiveWordRepositoryMongo } from '../../../infrastructure/repositories/offensive-word.repository.mongo';
import { OffensiveWordRequest } from '../offensive-word.request';
import { UpdateOffensiveWordUseCase } from './../update-offensive-word.usecase';
describe('Update offensive word use case', () => {

    it('should update an offensive word', async () => {

        const repository = new OffensiveWordRepositoryMongo();
        Container.set('OffensiveWordRepository', repository );

        const useCase = Container.get(UpdateOffensiveWordUseCase);
        const isUUID = '1a0fde8a-e773-40c4-a39e-701cc994da18';
        const offensiveWordRequest: OffensiveWordRequest = {
            word: 'chingada',
            level: 2
        };
        
        const response = await useCase.execute(isUUID, offensiveWordRequest);
        console.log('response de update en el test',response);

        expect(repository.update).toHaveBeenCalled();
        
    });
});