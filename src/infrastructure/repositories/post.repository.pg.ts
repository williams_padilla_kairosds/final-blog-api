import { Comment, CommentType } from '../../domain/entities/comment.entity';
import { Post, PostType } from '../../domain/entities/post.entity';
import { ExceptionWithCode } from '../../domain/execption-with-code';
import { PostRepository } from '../../domain/repositories/post.repository';
import { TextVO } from '../../domain/vos/author/text.vo';
import { TitleVO } from '../../domain/vos/author/title.vo';
import { CommentContentVO } from '../../domain/vos/comments/date.vo';
import { IdVO } from '../../domain/vos/id.vo';
import { NickNameVO } from '../../domain/vos/nickname.vo';
import { CommentModel } from './comment.schema';
import { PostModel } from './post.schema';
import { UserModel } from './user.schema';

export class PostRepositoryPG implements PostRepository {
    async createPost(userId: string, post: Post): Promise<Post> {
        const userModel: any = await UserModel.findOne({
            where: { id: userId },
        });

        if (!userModel) {
            throw new ExceptionWithCode(404, 'User not found');
        }

        const id = post.id;
        const author = post.authorName;
        const title = post.title;
        const text = post.text;

        const postModel: any = await userModel.createPost({
            id,
            author,
            title,
            text,
        });

        return new Post(this.toPostType(postModel));
    }

    async getAllPost(userId: string): Promise<Post[]> {
        const userModel: any = await UserModel.findOne({
            where: { id: userId },
        });
        if (!userModel) throw new ExceptionWithCode(404, 'User not found');

        const allPosts: any = await userModel.getPosts(); // add {include: CommentModel} to add Comments in request

        const POSTS: Post[] = allPosts.map((posts: any) => {
            return new Post(this.toPostType(posts));
        });

        console.log('--------> Sending response from Postgres <------------');
        return POSTS;
    }

    async getAllPublicPosts(): Promise<Post[]> {
        const postModel:any = await PostModel.findAll();
        if (!postModel) throw new ExceptionWithCode(404, 'Posts not found');

        console.log('--------> Sending response from Postgres <------------');
        return postModel.map((posts: any) => {
            return new Post(this.toPostType(posts));
        });
    }

    async getById( id: IdVO): Promise<Post> {

        const postModel:any = await PostModel.findOne({ where: { id: id.value }, include: 'comments' });
        if (!postModel) throw new ExceptionWithCode(404, 'Post not found');

        const allComments = postModel.comments.map((comment: any) => {
            return {
                id: comment.id,
                authorId:comment.userId,
                author: comment.author,
                content: comment.content,
            };
        });

        return new Post({
            id: IdVO.createWithUUID(postModel.id),
            authorName: NickNameVO.create(postModel.author),
            title: TitleVO.create(postModel.title),
            text: TextVO.create(postModel.text),
            comment: allComments.map((comment: any) => {
                return new Comment({
                    id: IdVO.createWithUUID(comment.id),
                    authorId: IdVO.createWithUUID(comment.authorId),
                    author: NickNameVO.create(comment.author),
                    content: CommentContentVO.create(comment.content),
                });
            }),
        });
    }

    async delete(userId: string, id: IdVO): Promise<IdVO> {
        const userModel = await UserModel.findOne({
            where: { id: userId },
        });
        if (!userModel) throw new ExceptionWithCode(404, 'User not found');

        const postModel = await PostModel.findOne({
            where: { id: id.value },
        });
        if (!postModel) throw new ExceptionWithCode(404, 'Post not found');

        postModel.destroy();
        return id;
    }

    async updatePost(userId: string, post: Post): Promise<Post> {
        const userModel = await UserModel.findOne({
            where: { id: userId },
        });
        if (!userModel) throw new ExceptionWithCode(404, 'User not found');

        const id = post.id;
        const title = post.title;
        const text = post.text;

        const postModel = await PostModel.update(
            { title, text },
            { where: { id: id } }
        ).then(() => {
            return PostModel.findOne({ where: { id: id } });
        });
        if (!postModel) throw new ExceptionWithCode(404, 'Post not found');

        return new Post(this.toPostType(postModel));
    }

    private toPostType(post: any): PostType {
        return {
            id: IdVO.createWithUUID(post.id),
            authorName: NickNameVO.create(post.author),
            title: TitleVO.create(post.title),
            text: TextVO.create(post.text),
        };
    }

    //--------------------------------------> Comment CRUD repository <----------------------------------//

    async createComment(
        userId: string,
        postId: string,
        comment: Comment
    ): Promise<Comment> {
        const userModel = await UserModel.findOne({ where: { id: userId } });
        if (!userModel) throw new ExceptionWithCode(404, 'User not found');

        const postModel: any = await PostModel.findOne({
            where: { id: postId },
        });
        if (!postModel) throw new ExceptionWithCode(404, 'Post not found');

        const { id, author, content } = comment;

        const commentModel = await postModel.createComment({
            id,
            author,
            content,
            userId,
        });

        return new Comment(this.toCommentType(commentModel));
    }

    async getAllComments(userId: string, postId: string): Promise<Comment[]> {
        const userModel = await UserModel.findOne({
            where: { id: userId },
        });
        if (!userModel) throw new ExceptionWithCode(404, 'User not found');

        const postModel: any = await PostModel.findOne({
            where: { id: postId },
        });
        if (!postModel) throw new ExceptionWithCode(404, 'Post not found');

        const allComments = await postModel.getComments();
        if (!allComments)
            throw new ExceptionWithCode(404, 'Comments not found');

        return allComments.map((comment: any) => {
            return new Comment(this.toCommentType(comment));
        });
    }

    async getCommentById(
        userId: string,
        postId: string,
        id: IdVO
    ): Promise<Comment> {
        const userModel = await UserModel.findOne({
            where: { id: userId },
        });
        if (!userModel) throw new ExceptionWithCode(404, 'User not found');

        const postModel: any = await PostModel.findOne({
            where: { id: postId },
        });
        if (!postModel) throw new ExceptionWithCode(404, 'Post not found');

        const commentModel: any = await CommentModel.findOne({
            where: { id: id.value },
        });
        return new Comment({
            id: IdVO.createWithUUID(commentModel.id),
            authorId: IdVO.createWithUUID(commentModel.userId),
            author: NickNameVO.create(commentModel.author),
            content: CommentContentVO.create(commentModel.content),
        });
    }

    async deleteComment(
        userId: string,
        postId: string,
        commentId: IdVO
    ): Promise<IdVO> {
        const userModel = await UserModel.findOne({
            where: { id: userId },
        });
        if (!userModel) throw new ExceptionWithCode(404, 'User not found');

        const postModel = await PostModel.findOne({
            where: { id: postId },
        });
        if (!postModel) throw new ExceptionWithCode(404, 'Post not found');

        const commentModel = await CommentModel.findOne({
            where: { id: commentId.value },
        });
        if (!commentModel)
            throw new ExceptionWithCode(404, 'Comment not found');

        
        commentModel.destroy();
        return commentId;
    }

    async updateComment(
        userId: string,
        postId: string,
        comment: Comment
    ): Promise<Comment> {
        const userModel = await UserModel.findOne({
            where: { id: userId },
        });
        if (!userModel) throw new ExceptionWithCode(404, 'User not found');

        const postModel = await PostModel.findOne({
            where: { id: postId },
        });
        if (!postModel) throw new ExceptionWithCode(404, 'Post not found');

        const id = comment.id;
        const content = comment.content;

        const commentModel = await CommentModel.update(
            { content },
            { where: { id: id } }
        ).then(() => {
            return CommentModel.findOne({ where: { id: id } });
        });

        if (!commentModel)
            throw new ExceptionWithCode(404, 'Comment not found');

        return new Comment(this.toCommentType(commentModel));
    }

    private toCommentType(comment: any): CommentType {
        return {
            id: IdVO.createWithUUID(comment.id),
            authorId: IdVO.createWithUUID(comment.userId),
            author: NickNameVO.create(comment.author),
            content: CommentContentVO.create(comment.content),
        };
    }
}
