import mongoose from 'mongoose';

const OffensiveWordSchema = new mongoose.Schema({
    idOf: {
        type: String,
        required: true
    },
    wordOf: {
        type: String,
        required: true
    },
    levelOf: {
        type: Number,
        required: true
    }
});

const OffensiveWordModel = mongoose.model('OffensiveWords', OffensiveWordSchema);



export { OffensiveWordModel };
