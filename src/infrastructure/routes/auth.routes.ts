import express from 'express';
import { body, validationResult } from 'express-validator';
import passport from 'passport';
import Container from 'typedi';
import { SignInUseCase } from '../../application/usecases/sign-in/sign-in.usecase';
import { SignUpUseCase } from '../../application/usecases/sign-up/sign-up.usecase';
import { User } from '../../domain/entities/user.entity';

const router = express.Router();

router.post(
    '/login',
    body('email').notEmpty(),
    body('password').notEmpty(),
    async (req: express.Request, res: express.Response) => {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty) return res.send(400).json(errors.array());

            const useCase = Container.get(SignInUseCase);
            const { email, password } = req.body;
            const token = await useCase.execute({ email, password });

            if (token) return res.status(200).json({ token });
            else res.status(401).json({ error: 'Not allowed' });
        } catch (error) {
            return res.status(400).json({ error: 'bad request' });
        }
    }
);

router.post(
    '/sign-up',
    body('email').notEmpty(),
    body('password').notEmpty(),
    async (req: express.Request, res: express.Response) => {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty)
                return res.status(400).json({ errors: errors.array() });

            const useCase = Container.get(SignUpUseCase);
            const { email, password, role, nickName } = req.body;
            console.log('data in sign  up nroute',email, password, role);
            await useCase.execute({ email, password, role, nickName });
            res.status(201).json({ status: 'Created' });
        } catch (error) {
            return res.status(400).json({ error: ' bad request' });
        }
    }
);

router.get(
    '/auth/role/me',
    passport.authenticate('jwt', { session: false }),
    (req, res) => {
        const user = req.user as User;
        res.status(200).json({ role: user.role.value, id: user.id.value });
    }
);

router.get('/status', (req, res) => {
    return res.status(200).json({message: 'hola'});
});

export { router as authRouter };
