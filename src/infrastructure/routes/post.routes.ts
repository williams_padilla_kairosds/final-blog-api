import express from 'express';
import passport from 'passport';
import Container from 'typedi';
import { CommentInput } from '../../application/usecases/comments/comment.interface';
import { CreateCommentUseCase } from '../../application/usecases/comments/create-comment.usecase';
import { DeleteCommentUseCase } from '../../application/usecases/comments/delete-comment.usecase';
import { GetAllCommentsUseCase } from '../../application/usecases/comments/get-all-comment.usecase';
import { GetCommentByIdUseCase } from '../../application/usecases/comments/get-by-id-comment.usecase';
import { UpdateCommentUseCase } from '../../application/usecases/comments/update-comment.usecase';
import { CreatePostUseCase } from '../../application/usecases/posts/create-post.usecase';
import { DeletePostUseCase } from '../../application/usecases/posts/delete-post.usecase';
import { GetAllPostsUseCase } from '../../application/usecases/posts/get-all-post.usecase';
import { GetAllPublicPostsUseCase } from '../../application/usecases/posts/get-all-public-post.usecase';
import { GetPostByIdUseCase } from '../../application/usecases/posts/get-post-by-id.usecase';
import { PostRequest } from '../../application/usecases/posts/post.interfaces';
import { UpdatePostUseCase } from '../../application/usecases/posts/update-post.usecase';
import { User } from '../../domain/entities/user.entity';
import { ExceptionWithCode } from '../../domain/execption-with-code';
import { Role } from '../../domain/vos/role.vo';
import { cacheMiddleWare } from '../cache/cache-middleware';
import { canUpdate } from '../middlewares/canUpdate';
import { hasRole } from '../middlewares/roles';

const router = express.Router();

//create a post
router.post(
    '/post',
    passport.authenticate('jwt', { session: false }),
    hasRole([Role.ADMIN, Role.AUTHOR]),
    cacheMiddleWare(),  

    async (req, res) => {
        try {
            const user = req.user as User;

            const author = user.nickName.value;
            const { title, text } = req.body;
            const postRequest: PostRequest = { author, title, text };
            const useCase = Container.get(CreatePostUseCase);
            const response = await useCase.execute(user.id.value, postRequest);
            return res.status(201).send(response);
        } catch (error) {
            if (error instanceof ExceptionWithCode) {
                return res.status(error.code).json({ error: error.message });
            } else {
                return res.sendStatus(404).json(error);
            }
        }
    }
);
//get all public post from all users
router.get(
    '/post/public',
    // passport.authenticate('jwt', { session: false }),
    // hasRole([Role.ADMIN, Role.AUTHOR, Role.USER]),
    cacheMiddleWare(),
    async (req, res) => {
        try {
            const useCase = Container.get(GetAllPublicPostsUseCase);
            const response = await useCase.execute();
            res.locals.data = response.toString();

            return res.status(200).send(response);
        } catch (error) {
            if (error instanceof ExceptionWithCode) {
                return res.status(error.code).send({ error: error.message });
            } else {
                return res.status(404).json({ error: error });
            }
        }
    }
);

//get all post from logged user
router.get(
    '/post',
    passport.authenticate('jwt', { session: false }),
    hasRole([Role.ADMIN, Role.AUTHOR]),
    async (req, res) => {
        try {
            const useCase = Container.get(GetAllPostsUseCase);
            const user = req.user as User;
            const response = await useCase.execute(user.id.value);
            return res.status(200).send(response);
        } catch (error) {
            if (error instanceof ExceptionWithCode) {
                return res.status(error.code).send({ error: error.message });
            } else {
                return res.status(404).json({ error: error });
            }
        }
    }
);

//get a single post with its comments
router.get(
    '/post/:id',
    // passport.authenticate('jwt', { session: false }),
    // hasRole([Role.ADMIN, Role.AUTHOR, Role.USER]),
    async (req, res) => {
        try {
            const { id } = req.params;
            const useCase = Container.get(GetPostByIdUseCase);
            const response = await useCase.execute( id);
            return res.status(200).send(response);
        } catch (error) {
            if (error instanceof ExceptionWithCode) {
                return res.status(error.code).json({ error: error.message });
            } else {
                return res.status(404).json({ error: error });
            }
        }
    }
);

//delete a post from logged user
router.delete(
    '/post/:id',
    passport.authenticate('jwt', { session: false }),
    hasRole([Role.ADMIN, Role.AUTHOR]),
    cacheMiddleWare(),
    async (req, res) => {
        try {
            const { id } = req.params;
            const user = req.user as User;
            const useCase = Container.get(DeletePostUseCase);
            const response = await useCase.execute(user.id.value, id);

            return res.status(200).json({ id: response });
        } catch (error) {
            if (error instanceof ExceptionWithCode) {
                return res.status(error.code).json({ error: error.message });
            } else {
                return res.status(400).json({ error });
            }
        }
    }
);

//update a post from logged user
router.patch(
    '/post/:id',
    passport.authenticate('jwt', { session: false }),
    hasRole([Role.ADMIN, Role.AUTHOR]),
    cacheMiddleWare(),

    async (req, res) => {
        try {
            const user = req.user as User;
            const author = user.nickName.value;
            const { id } = req.params;
            const { title, text } = req.body;

            const postInput: PostRequest = { author, title, text };
            const useCase = Container.get(UpdatePostUseCase);
            const response = await useCase.execute(
                user.id.value,
                id,
                postInput
            );

            return res.status(200).send(response);
        } catch (error) {
            if (error instanceof ExceptionWithCode) {
                return res.status(error.code).json({ error: error.message });
            } else {
                return res.status(400).json({ error: error });
            }
        }
    }
);

//--------------------------------------> Comment CRUD Routes <----------------------------------//

//add new comment to an existing post
router.post(
    '/post/:postId/comments',
    passport.authenticate('jwt', { session: false }),
    hasRole([Role.ADMIN, Role.AUTHOR, Role.USER]),
    async (req, res) => {
        try {
            const user = req.user as User;
            const author = user.nickName.value;

            const { postId } = req.params;
            const { content } = req.body;

            const input: CommentInput = { author, content };
            const useCase = Container.get(CreateCommentUseCase);
            const response = await useCase.execute(
                user.id.value,
                postId,
                input
            );

            return res.status(201).send(response);
        } catch (error) {
            if (error instanceof ExceptionWithCode) {
                return res.status(error.code).json({ error: error.message });
            } else {
                return res.status(400).json({ error: error });
            }
        }
    }
);

//all comments from a post
router.get(
    '/post/:postId/comments',
    passport.authenticate('jwt', { session: false }),
    hasRole([Role.ADMIN, Role.AUTHOR, Role.USER]),
    async (req, res) => {
        try {
            const user = req.user as User;

            const { postId } = req.params;
            const useCase = Container.get(GetAllCommentsUseCase);

            const response = await useCase.execute(user.id.value, postId);
            console.log('respons dentro del reouter', response);
            return res.status(200).send(response);
        } catch (error) {
            if (error instanceof ExceptionWithCode) {
                return res.status(error.code).json({ error: error.message });
            } else {
                return res.status(400).json({ error: error });
            }
        }
    }
);

//get a commend by ID
router.get(
    '/post/:postId/comments/:commentId',
    passport.authenticate('jwt', { session: false }),
    hasRole([Role.ADMIN, Role.AUTHOR, Role.USER]),
    async (req, res) => {
        try {
            const user = req.user as User;
            const { postId, commentId } = req.params;

            const useCase = Container.get(GetCommentByIdUseCase);
            const response = await useCase.execute(
                user.id.value,
                postId,
                commentId
            );

            return res.status(201).send(response);
        } catch (error) {
            if (error instanceof ExceptionWithCode) {
                return res.status(error.code).json({ error: error.message });
            } else {
                return res.status(400).json({ error: error });
            }
        }
    }
);

router.delete(
    '/post/:postId/comments/:commentId',
    passport.authenticate('jwt', { session: false }),
    hasRole([Role.ADMIN, Role.AUTHOR, Role.USER]),
    canUpdate(),
    async (req, res) => {
        try {
            const user = req.user as User;
            const { postId, commentId } = req.params;

            const useCase = Container.get(DeleteCommentUseCase);
            const response = await useCase.execute(
                user.id.value,
                postId,
                commentId
            );
            console.log('response del delete en route', response);
            return res.status(200).send({ id: response });
        } catch (error) {
            if (error instanceof ExceptionWithCode) {
                return res.status(error.code).json({ error: error.message });
            } else {
                return res.status(400).json({ error: error });
            }
        }
    }
);

router.patch(
    '/post/:postId/comments/:commentId',
    passport.authenticate('jwt', { session: false }),
    hasRole([Role.ADMIN, Role.AUTHOR ,Role.USER]),
    canUpdate(),
    async (req, res) => {
        try {

            const user = req.user as User;
            const author = user.nickName.value;

            const { postId, commentId } = req.params;
            const { content } = req.body;

            const input: CommentInput = { author, content };

            const useCase = Container.get(UpdateCommentUseCase);
            const response = await useCase.execute(user.id.value, postId, commentId, input );

            return res.status(200).send(response);
            
        } catch (error) {
            if (error instanceof ExceptionWithCode) {
                return res
                    .status(error.code)
                    .json({ error: error.message });
            } else {
                return res.status(400).json({ error: error });
            }
        }
    }
);

export { router as postRouter };
