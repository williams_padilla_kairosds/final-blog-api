import express from 'express';
import { body, validationResult } from 'express-validator';
import passport from 'passport';
import Container from 'typedi';
import { CreateOffensiveWordUseCase } from '../../application/usecases/create-offensive-word.usecase';
import { DeleteOffensiveWordUseCase } from '../../application/usecases/delete-offensive-word.usecase';
import { GetAllOffensiveWordsUseCase } from '../../application/usecases/get-all-offensive-words.usecase';
import { GetOffensiveWordByIdUseCase } from '../../application/usecases/get-by-id-offensive-word.usecase';
import { OffensiveWordRequest } from '../../application/usecases/offensive-word.request';
import { UpdateOffensiveWordUseCase } from '../../application/usecases/update-offensive-word.usecase';
import { ExceptionWithCode } from '../../domain/execption-with-code';
import { Role } from '../../domain/vos/role.vo';
import { hasRole } from '../middlewares/roles';

const router = express.Router();

router.post(
    '/offensive-word',
    body('word').notEmpty().isString().isLength({ min: 1 }),
    body('level').notEmpty().isNumeric().isLength({ min: 1, max: 1 }),
    async (req: express.Request, res: express.Response) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }

        try {
            const { word, level } = req.body;
            const offensiveWordRequest: OffensiveWordRequest = { word, level };
            const useCase = Container.get(CreateOffensiveWordUseCase);
            const response = await useCase.execute(offensiveWordRequest);
            res.status(201).send(response);
        } catch (err) {
            if (err instanceof ExceptionWithCode)
                res.sendStatus(err.code).json({ error: err.message });
        }
    }
);

//getAll
router.get(
    '/offensive-word',
    passport.authenticate('jwt', { session: false }),
    hasRole([Role.ADMIN]),
    async (req, res) => {
        try {
            const useCase = Container.get(GetAllOffensiveWordsUseCase);
            const data = await useCase.execute();
            res.status(200).send(data);
        } catch (err) {
            console.log(err);
            if (err instanceof Error) {
                res.sendStatus(400).json({ error: err.message });
            }
        }
    }
);

router.get('/offensive-word/:id', async (req, res) => {
    const { id } = req.params;
    const useCase = Container.get(GetOffensiveWordByIdUseCase);
    const response = await useCase.execute(id);
    res.status(200).send(response);
});

router.delete('/offensive-word/:id', async (req, res) => {
    try {
        const { id } = req.params;
        const useCase = Container.get(DeleteOffensiveWordUseCase);
        const response = await useCase.execute(id);
        return res.status(200).send(response);
    } catch (err) {
        if (err instanceof ExceptionWithCode) {
            return res.status(err.code).json({ error: err.message });
        } else {
            return res.json({ error: 'Unexpected error' });
        }
    }
});

router.put('/offensive-word/:id', async (req, res) => {
    const { word, level } = req.body;
    const { id } = req.params;
    const offensiveWordRequest: OffensiveWordRequest = { word, level };
    const useCase = Container.get(UpdateOffensiveWordUseCase);
    const response = await useCase.execute(id, offensiveWordRequest);

    res.status(201).send(response);
});

export { router as offensiveWordRouter };
