import express from 'express';
import Container from 'typedi';
import { GetCommentByIdUseCase } from '../../application/usecases/comments/get-by-id-comment.usecase';
import { User } from '../../domain/entities/user.entity';

export const canUpdate = () => {
    return async (
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ): Promise<express.Response | void> => {

        const user = req.user as User;
        const { postId, commentId } = req.params;
        const useCase = Container.get(GetCommentByIdUseCase);
        const comment = await useCase.execute(user.id.value, postId, commentId);

        if(user.role.value !== 'ADMIN'){
            if(comment.authorId == user.id.value) next();
            else return res.status(403).json({error: 'Not allow to perform this action'});
        } else {
            next();
        }
        
    };
};
