import express from 'express';
import Container from 'typedi';
import { GetAllPublicPostsUseCase } from '../../application/usecases/posts/get-all-public-post.usecase';
import { CacheRepository } from './cache.respository.mongo';

export const cacheMiddleWare = () => {
    return async (
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ): Promise<express.Response | void> => {
        const cache = new CacheRepository();
        const url = req.originalUrl;

        if (req.method != 'GET') {
            await cache.drop();
            console.log('--------> Dropping cache <------------');
            res.on('finish', async () => {
                const useCase = Container.get(GetAllPublicPostsUseCase);
                const data = await useCase.execute();
                await cache.add(url,data);
            });
            next();
        } else {
            const cachePost:any = await cache.getAllCachePost();
            if (cachePost) {
                console.log('--------> Sending cache <------------');
                return res.status(200).json(JSON.parse(cachePost.posts));
            } else {
                res.on('finish', async () => {
                    const data = res.locals.data;
                    if (data) {
                        await cache.add(url, data);
                        console.log('res locals ', data);
                    }
                });
                next();
            }
        }
    };
};
