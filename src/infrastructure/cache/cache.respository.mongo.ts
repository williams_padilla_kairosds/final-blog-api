import { PostCacheModel } from '../repositories/post-cache.schema';

export class CacheRepository {
    async getAllCachePost() {
        const allCachePost = await PostCacheModel.findOne({
            url: '/api/post',
        });
        return allCachePost;
    }

    async add(url: string, data: any): Promise<void> {
        const newPost = new PostCacheModel({
            url: url,
            posts: JSON.stringify(data),
        });
        await newPost.save();
    }

    async drop(): Promise<void> {
        await PostCacheModel.deleteMany({});
    }
}
