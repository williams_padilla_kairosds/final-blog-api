import mongoose from 'mongoose';

const connectToDB = async () => {
    const host = process.env.MONGO_HOST ?? 'localhost';
    const port = process.env.MONGO_PORT ?? '27018';
    const dbName = process.env.MONGO_DB ?? 'blog';
    const user = process.env.MONGO_AUTH_USER ?? 'admin';
    const pass = process.env.MONGO_AUTH_PASS ?? 'admin';
    const url = `mongodb://${host}:${port}/${dbName}`;
    try {
        console.log(`>>>>>>>>>>>>>>> URL: ${url}<<<<<<<<<<<<<<<<<<<<<`);

        await mongoose.connect(url, {
            authSource: 'admin',
            auth: {
                username: user,
                password: pass,
            },
        });
        console.log('>>>>>>>>>>>>>>><conected to mongo<<<<<<<<<<<<<<<<<<<<<');
    } catch (error) {
        console.log(error);
    }
};

export { connectToDB };
