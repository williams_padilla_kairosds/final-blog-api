import Container from 'typedi';
import { OffensiveWordType } from '../../domain/entities/offensive-word.entity';
import { OffensiveWordService } from '../../domain/services/offensive-word.service';
import { IdVO } from '../../domain/vos/id.vo';
import { LevelVO } from '../../domain/vos/level.vo';
import { WordVO } from '../../domain/vos/word.vo';

const populate = async (): Promise<void> => {
    const offensiveWordsService = Container.get(OffensiveWordService);
    const offensiveWordsTypes: OffensiveWordType[] = [
        {
            id: IdVO.create(),
            word: WordVO.create('scumbag'),
            level: LevelVO.create(4),
        },
        {
            id: IdVO.create(),
            word: WordVO.create('cacotas'),
            level: LevelVO.create(3),
        },
        {
            id: IdVO.create(),
            word: WordVO.create('idiot'),
            level: LevelVO.create(3),
        },
    ];

    await offensiveWordsService.persist(offensiveWordsTypes[0]);
    await offensiveWordsService.persist(offensiveWordsTypes[1]);
    await offensiveWordsService.persist(offensiveWordsTypes[2]);
};

export { populate as populateOffensiveWords };

