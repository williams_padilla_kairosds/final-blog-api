import { Container } from 'typedi';
import { User, UserType } from '../../domain/entities/user.entity';
import { UserService } from '../../domain/services/user.service';
import { EmailVo } from '../../domain/vos/email.vo';
import { IdVO } from '../../domain/vos/id.vo';
import { NickNameVO } from '../../domain/vos/nickname.vo';
import { PasswordVo } from '../../domain/vos/password.entity';
import { Role, RoleVo } from '../../domain/vos/role.vo';

const populate = async ():Promise<void> => {
    const userService = Container.get(UserService);
    const adminUser: UserType = {
        id: IdVO.create(),
        email: EmailVo.create('willy-admin@hola.com'),
        password: PasswordVo.create('1234'),
        role: RoleVo.create(Role.ADMIN),
        nickName: NickNameVO.create('GodAdmin')
    };

    const authorUser: UserType = {
        id: IdVO.create(),
        email: EmailVo.create('willy-author@hola.com'),
        password: PasswordVo.create('1234'),
        role: RoleVo.create(Role.AUTHOR),
        nickName: NickNameVO.create('StarFlow')
    };

    const basicUser: UserType = {
        id: IdVO.create(),
        email: EmailVo.create('willy-user@hola.com'),
        password: PasswordVo.create('1234'),
        role: RoleVo.create(Role.USER),
        nickName: NickNameVO.create('DumbUser')
    };



    await userService.persist(new User(adminUser));
    await userService.persist(new User(authorUser));
    await userService.persist(new User(basicUser));
};

export { populate as populateDataBases };
