import { Sequelize } from 'sequelize';
import { populateDataBases } from './populate';

const host = process.env.PG_HOST ?? 'localhost';
const user = process.env.PG_USER ?? 'pguser';
const pass = process.env.PG_PASSWORD ?? 'pguser';
const dbName = process.env.PG_DB_NAME ?? 'pgdb';
const port = process.env.APP_PG_PORT ?? '5432';

const sequelize = new Sequelize(`postgres://${user}:${pass}@${host}:${port}/${dbName}`);

sequelize
    .authenticate()
    .then(() => {
        console.log('Connection Success');
    })
    .catch((err) => console.log(err));

sequelize.sync({ force: true }).then(() => {
    console.log('Database and table created');
    populateDataBases();
});

export default sequelize;
