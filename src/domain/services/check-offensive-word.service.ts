import Container from 'typedi';
import { GetAllOffensiveWordsUseCase } from '../../application/usecases/get-all-offensive-words.usecase';

export const hasOffensiveWord = async (input: string) => {
    const useCase = Container.get(GetAllOffensiveWordsUseCase);
    const response = await useCase.execute();
    const offensiveWords = response.map((offensiveWord) => {
        return offensiveWord.word.toLocaleLowerCase();
    });    
    
    const words = input.toLocaleLowerCase().split(' ');
    
    let offensiveWordsFound: Array<string> = [];
    
    words.forEach((word) => {
        if (offensiveWords.includes(word)) {
            offensiveWordsFound = [...offensiveWordsFound, word];
        }
    });

    return offensiveWordsFound;
};

