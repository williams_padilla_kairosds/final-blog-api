import { ExceptionWithCode } from '../../execption-with-code';

export class CommentNickNameVO {
    get value(): string {
        return this.commentNickName;
    }

    private constructor(private commentNickName: string) {}

    static create(commentNickName: string): CommentNickNameVO {
        if (commentNickName.length < 5 || commentNickName.length > 30) {
            throw new ExceptionWithCode(
                400,
                `${commentNickName} is not valid, It length must be between 5 and 30`
            );
        }

        return new CommentNickNameVO(commentNickName);
    }
}
