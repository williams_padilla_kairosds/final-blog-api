import { Comment } from '../entities/comment.entity';
import { Post } from '../entities/post.entity';
import { IdVO } from '../vos/id.vo';

export interface PostRepository {

    createPost(userId:string, post: Post): Promise<Post>
    
    getAllPost(userId: string): Promise<Post[]> 

    delete(userId: string, id:IdVO): Promise<IdVO>
    
    getById( id:IdVO): Promise<Post>

    updatePost(userId:string, post:Post): Promise<Post>

    getAllPublicPosts(): Promise<Post[]>

    // ------------> Comments crud <--------------//

    createComment(userId: string, postId:string, comment: Comment): Promise<Comment>

    getAllComments(userId: string, postId: string): Promise<Comment[]>

    getCommentById(userId:string, postId: string, id:IdVO): Promise<Comment>

    deleteComment(userId: string, postId: string, commentId: IdVO) : Promise<IdVO>

    updateComment(userId:string, postId: string, comment: Comment) : Promise<Comment>
}