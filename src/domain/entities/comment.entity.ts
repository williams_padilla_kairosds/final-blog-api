import { CommentContentVO } from '../vos/comments/date.vo';
import { IdVO } from '../vos/id.vo';
import { NickNameVO } from '../vos/nickname.vo';

export type CommentType = {
    id: IdVO;
    authorId?: IdVO;
    author: NickNameVO;
    content: CommentContentVO;
}

export class Comment {

    constructor(private comment: CommentType) {}

    get id(): string {
        return this.comment.id.value;
    }

    get authorId(): string | undefined{
        return this.comment.authorId?.value;
    }

    get author(): string {
        return this.comment.author.value;
    }

    get content(): string {
        return this.comment.content.value;
    }
    
}