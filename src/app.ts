import 'reflect-metadata';

import { json } from 'body-parser';
import cors from 'cors';
import express from 'express';
import passport from 'passport';
import { Container } from 'typedi';
import { connectToDB } from './infrastructure/config/mongo';
import { populateOffensiveWords } from './infrastructure/config/populates-OffensiveWord';
import './infrastructure/config/postgresql';
import passportMiddleware from './infrastructure/middlewares/passport';
import { userRelation } from './infrastructure/relations/user-post.relation';
import { OffensiveWordRepositoryMongo } from './infrastructure/repositories/offensive-word.repository.mongo';
import { PostRepositoryPG } from './infrastructure/repositories/post.repository.pg';
import { UserRepositoryPG } from './infrastructure/repositories/user-repository.pg';
import { authRouter } from './infrastructure/routes/auth.routes';
import { offensiveWordRouter } from './infrastructure/routes/offensive-word.routes';
import { postRouter } from './infrastructure/routes/post.routes';
Container.set('OffensiveWordRepository', new OffensiveWordRepositoryMongo());
Container.set('UserRepository', new UserRepositoryPG());
Container.set('PostRepository', new PostRepositoryPG);
connectToDB();
populateOffensiveWords();

const app = express();


app.use(json());
app.use(cors());
app.use('/api',offensiveWordRouter);
app.use('/api',authRouter);
app.use('/api',postRouter);
app.use(passport.initialize());
passport.use(passportMiddleware);

app.listen(3000,  () =>{ 
    
    console.log('Server started');
    userRelation();


});

console.log('App started');